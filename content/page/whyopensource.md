---
title: Why use Open Source Software
subtitle: Benifits of using Open Source Software
comments: false
slug: whyopensource
---

**NOTICE:** The use of term `nonfree` on this page does not refer to the price, but refers instead
to the freedom (or more specifically the lack theirof) offered by the program and/or its developer(s).
Learn more at https://www.gnu.org/philosophy/categories.en.html#non-freeSoftware

It is my belief that everyone should use Free Libre Open Source (FLOSS) software. I use FLOSS software
for its cost (or lack theirof), freedom, and privacy. (FLOSS) software is free to use, provides the
ability for me to modify it for any reasons, and respects my privacy. I also find that FLOSS software
is generally more compatible with Linux which is my preferred Operating System.

Those are my reasons for using FLOSS software. You may have other things that you care about, and
that is fine. There are many reasons you may want to use FLOSS. These may be anything from Cost
reasons to privacy reasons to not wanting to support big tech companies. Here is a short list of
reasons you may want to use FLOSS software, although there are most certainly others.

**NOTICE:** The use of non-free 

 * Cost - If you don't have any excess money to spend, don't have access to a credit card, or don't
   or can't pay for software for some other reason, FLOSS software may be of interest to you.
 * Freedom - True FLOSS software is designed to give you control and stay out of your way, let you
   hack on it in anyway you want, and not force you to use it in the way the software was intended
   to be used. Additionally, open source software does not generally attempt to lock you in to their
   programs, or restrict you to whatever platform they say you can use their app on. For instance,
   you will never find an FLOSS web service that forces or even pushes you to use an app to access
   their service on mobile devices.
 * Security - Open Source Software in general, and FLOSS software in particular is generally more
   secure. While it is commonly said that this is due to their being more eyes on the software, this
   is not necessary the reason. With non open-source software, you are relying the software developer(s)
   to (A) be aware of any security issues (it is uncommon for their to be an official way to make devs
   aware of security issues), and (B) fix the issues. With FLOSS software specifically, "anyone"
   {{< sup >}}1{{</ sup >}} is able to fix the issue and contribute the fix back to the developers
   of the software. Additionally, open source developers are usually not tied to a rigid release
   schedule, meaning that they are often able to get the fixed released almost immediately.
 * Privacy - It is common for non-free software to not respect your privacy, and instead track you,
   collect and store personally identifiable information, and send that data to third parties (often
   without your knowledge. Most free software projects go out of their way to collect as little data
   about you as possible. Exceptions to this norm include open source projects by companies like
   f@ceb00k and g00gl3.
 * Features - I have been amazed by how many times I have used an FLOSS tool or application (or even
   a set of tools or applications) and have found it to have more features, customization, or even
   documentation then a non-free alternative. And it may not even be a new feature, but a feature that
   works better on the FLOSS version compared to the non-free alternative. It isn't even small things.
   I have seen features that are so obvious that I go to use the non-free version for some reason
   and fail to understand why that feature does not exist. Now this is not always the case. I have
   seen instances of a open source alternative to a non-free project be missing several features that
   are in the non-free one. However, in a majority of cases, those features are either being worked
   on, or are on the projects roadmap. Very rarely have I seen the developers refuse to implement the
   feature or refuse to take contributions that implement that feature.

Now I will not claim that using FLOSS or even OSS (open source software) tools and applications is
perfect. Open source projects can be quite buggy or not work at all. It is also not that uncommon to
find an interesting project, but have it be unmatained or unsupported. However, finding a actively
worked on project that does everything that you need (or not, you might need multiple apps to do
everything you need, or decide that you can deal with missing features), can be quite rewarding.

{{< sup >}}1{{</ sup >}} While technically anyone is able to make modifications to open source software,
it requires at least a basic knowledge of whatever programming language(s) are used by the project.
