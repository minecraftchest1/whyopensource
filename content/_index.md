## What is this

This site was created for a School Project. I was instructed to create a website to solve a problem
that I have. My problem was the dificulty that I have convincing others to FLOSS software. this site
contains a list of FLOSS software I use with a brief description of what it is, some features, benifits,
caveats, and a link to get it.

In addition, I also have some reasons why you should use FLOSS software, why you should avoid nonfree
software, and perhaps some other things as well. While I may update it down the line, I will make no
guatentees.
