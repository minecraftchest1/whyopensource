---
title: "Libreoffice"
date: 2023-02-27T20:09:16Z
tags:
  - desktop
  - office
categories:
  - productivity
image: /post/libreoffice/1.png
---

## Info

| Name        | Platforms                | Tags                   | Notes                                  |
|-------------|--------------------------|------------------------|----------------------------------------|
| Libreoffice | [desktop](/tags/desktop) | [Office](/tags/matrix) | Compatible with Microsoft Office files |
|             |                          | [Productivity](/categories/productivity) |                      |


## Description

Libreoffice is a fully featured Office Productivity suite, compatible with Microsoft Office files. Runs on Windows, Mac, and Linux

## Features

  - Document editing
  - Presentations
  - Spreadsheets
  - Databases
  - Cloud sync using various cloud storage providers
  - And more

## Screenshots
{{< gallery >}}
  {{< figure src="/post/libreoffice/1.png" >}}
{{< /gallery >}}