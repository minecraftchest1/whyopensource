---
title: "Element"
date: 2023-02-27T20:09:16Z
tags:
  - mobile
  - desktop
  - web
  - collaberation
  - matrix
categories:
  - chat
image: /post/element/3.png
---

## Info

| Name     | Platforms               | Tags                     | Notes |
|------|-----------|------|-------|
| Element | [mobile](/tags/mobile)   | [Matrix](/tags/matrix)   | Uses the [matrix](https://matrix.org) standard. |
|         | [web](/tags/web)         | [Chat](/categories/chat) | |
|         | [desktop](/tags/desktop) |                          | |

## Description

Element is a multi-platform app for real-time communications using the Matrix specifications.

## Features

  - Group messaging
  - Private messaging
  - End to End encryption
  - File attachments
  - Rich text formatting using markdown
  - Sharing of link to chat
  - Syncronizing of messages between devices
  - Reactions using emojis
  - Compatability with all [Matrix](https://matrix.org) clients
  - And more

## Screenshots
{{< gallery >}}
  {{< figure src="/post/element/1.png" >}}
  {{< figure src="/post/element/2.png" >}}
  {{< figure src="/post/element/3.png" >}}
{{< /gallery >}}

## Get

https://element.io/download#downloads
